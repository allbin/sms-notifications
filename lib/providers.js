let request = require('request');
let queryString = require('query-string');

module.exports = {
    spirius: {
        send: (opts, recipients, msg) => {
            return new Promise((resolve, reject) => {
                if (!Array.isArray(recipients) || recipients.length === 0) {
                    return resolve();
                }
                let params = {
                    User: opts.username,
                    Pass: opts.password,
                    From: opts.from,
                    FromType: opts.from.startsWith('+') ? 'I': 'A',
                    To: recipients.join(';'),
                    Msg: msg,
                    CharSet: 'UTF-8'
                };
                let qs = queryString.stringify(params);
                let req_opts = {
                    method: 'GET',
                    uri: 'https://get.spiricom.spirius.com:55001/cgi-bin/sendsms?' + qs
                };

                request(req_opts, (err, res, body) => {
                    if (err) {
                        return reject(err);
                    }
                    if (res.statusCode !== 202) {
                        return reject(new Error("Provider responded with HTTP " + res.statusCode + ", body: " + body));
                    }
                    return resolve();
                });
            });
        }
    }
};

