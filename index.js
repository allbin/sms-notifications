let opts_defaults = {
    providers: require('./lib/providers'),
    provider: "spirius"
};

let SMS = function(opts) {
    this.opts = Object.assign({}, opts_defaults, opts || {});
    this.provider = this.opts.providers.hasOwnProperty(this.opts.provider) ? this.opts.providers[this.opts.provider] : null;

    return {
        init: () => {
            return new Promise((resolve, reject) => {
                if (!this.provider) {
                    setImmediate(() => {
                        return reject(new Error("Invalid provider: " + this.opts.provider));
                    });
                }
                let all_opts = Object.keys(this.opts);
                let missing_mandatory_opts = ["username", "password", "from"].filter(x => !all_opts.includes(x));
                if (missing_mandatory_opts.length > 0) {
                    setImmediate(() => {
                        return reject(new Error("Missing mandatory parameter(s): " + missing_mandatory_opts.join()));
                    });
                }
                setImmediate(() => {
                    return resolve();
                });
            });
        },
        send: (recipients, msg) => {
            return this.provider.send(this.opts, recipients, msg);
        }
    };
};

module.exports = SMS;
