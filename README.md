# sms-notifications

## Example usage
```
let SMS = require('sms-notifications')

let sms = new SMS({
    username: 'myusername',
    password: 'mypassword',
    provider: <provider> (default: "spirius")
    from: <number|name> (format depends on provider)
});

let recipients = ["+46709123123", "+46709124124"];
let body = "Hello!";

sms.send(recipients, body)
    .then(() => {
        console.log("Message sent!");
    })
    .catch((err) => {
        console.error("Something went wrong", err.stack);
    });
```

